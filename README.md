
NGINX proxy for sit-xpt.udev.hk
===

Usage
---

```sh
docker-compose up -d
```

SSL cert
---

SSL cert and key must be put under `/etc/ssl/xpt` and named `xpt.udev.hk.crt`
and `xpt.udev.hk.key`.


Ports
---

Ports 80, 443, 8051 are exposed on the host. HTTP connections to port 80 will
be redirected to port 443 of the same virtual host; port 8051 is used only
for proxying gRPC traffics (unencrypted).

gRPC
---

Containers exposing gRPC port must set the environment variable `VIRTUAL_PROTO`
to `grpc` to enable gRPC proxying. `VIRTUAL_PORT` should also be set if the
actual port in use is different from the port(s) exposed by the image.

Networks
---

The daemon only proxy requests to containers connected to the `xpoint` and 
`bitcoin` bridge networks.

